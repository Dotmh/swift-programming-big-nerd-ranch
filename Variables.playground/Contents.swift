//: Playground - noun: a place where people can play

import Cocoa

let numberOfSpotlights: Int = 4
var population: Int
var levelOfUnemployment: Int = 598
population = 5422
let townName: String = "Knowhere"
let townDescription = "\(townName) has a population of \(population) of which \(levelOfUnemployment) are unemployed and \(numberOfSpotlights) stoplights."

print(townDescription)
